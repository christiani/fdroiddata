Categories:System
License:Apache2
Web Site:
Source Code:https://github.com/SimpleMobileTools/Simple-Flashlight
Issue Tracker:https://github.com/SimpleMobileTools/Simple-Flashlight/issues
Changelog:https://github.com/SimpleMobileTools/Simple-Flashlight/blob/HEAD/CHANGELOG.md

Auto Name:Flashlight
Summary:A simple flashlight with a widget
Description:
A flashlight app with black background, so it doesn't knock you out when
launched in the dark. If it's turned on via the app, it will prevent the device
from falling asleep, so it can be used for longer operations too. It also
contains a simple 1x1 widget you can place on your homescreen.
.

Repo Type:git
Repo:https://github.com/SimpleMobileTools/Simple-Flashlight

Build:1.8,8
    commit=3562a37fd0e968f547e3d87388a4bd624a8c7370
    subdir=app
    gradle=yes

Build:1.9,9
    commit=1.9
    subdir=app
    gradle=yes

Build:1.11,11
    commit=1.11
    subdir=app
    gradle=yes

Build:1.13,13
    commit=1.13
    subdir=app
    gradle=yes

Build:1.14,14
    commit=1.14
    subdir=app
    gradle=yes

Build:1.15,15
    commit=1.15
    subdir=app
    gradle=yes

Build:1.17,17
    commit=1.17
    subdir=app
    gradle=yes

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:1.17
Current Version Code:17
